module "cluster-1-nodepool-1" {
  for_each     = var.nodepool
  source       = "git::https://github.com/GoogleCloudPlatform/cloud-foundation-fabric.git//modules/gke-nodepool?ref=v29.0.0"
  project_id   = data.google_project.project.project_id
  cluster_name = module.cluster-1.name
  location     = var.zone
  name         = each.key
  labels       = { environment = "dev" }
  service_account = {
    create       = true
    email        = each.key
    oauth_scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }
  node_config = {
    machine_type        = "n2-standard-2"
    disk_size_gb        = 50
    disk_type           = "pd-ssd"
    ephemeral_ssd_count = 1
    gvnic               = true
    spot                = true
  }
  nodepool_config = {
    autoscaling = {
      max_node_count = each.value.max_node_count
      min_node_count = each.value.min_node_count
    }
    management = {
      auto_repair  = true
      auto_upgrade = true
    }
  }
}
