variable "project" {
  type        = string
  description = "Id of Google cloud project to use"
}
variable "region" {
  type        = string
  description = "Region to use"
}
variable "zone" {
  type        = string
  description = "Zone to use"
}
variable "network" {
  type        = string
  description = "Network to use"
}
variable "subnetwork" {
  type        = string
  description = "Subnetwork to use"
}
variable "cluster_name" {
  type        = string
  description = "Name"
}
variable "nodepool" {
  type = map(object({
    min_node_count = optional(number, 1)
    max_node_count = optional(number, 2)
  }))
}
