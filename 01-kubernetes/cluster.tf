data "google_compute_network" "vpc" {
  name    = var.network
  project = var.project
}

data "google_compute_subnetwork" "subnet" {
  name    = var.subnetwork
  project = var.project
  region  = var.region
}

resource "google_project_service" "project" {
  project = data.google_project.project.project_id
  service = "container.googleapis.com"

  timeouts {
    create = "30m"
    update = "40m"
  }

  disable_dependent_services = false
}

resource "google_project_iam_member" "gke_service" {
  role    = "roles/container.serviceAgent"
  member  = "serviceAccount:service-${data.google_project.project.number}@container-engine-robot.iam.gserviceaccount.com"
  project = data.google_project.project.project_id
}
resource "google_project_iam_member" "cloudservices" {
  role    = "roles/editor"
  member  = "serviceAccount:${data.google_project.project.number}@cloudservices.gserviceaccount.com"
  project = data.google_project.project.project_id
}

# Create a GKE cluster
module "cluster-1" {
  source              = "git::https://github.com/GoogleCloudPlatform/cloud-foundation-fabric.git//modules/gke-cluster-standard?ref=v29.0.0"
  project_id          = data.google_project.project.project_id
  name                = var.cluster_name
  location            = var.zone
  deletion_protection = false
  release_channel     = "REGULAR"
  vpc_config = {
    network    = data.google_compute_network.vpc.self_link
    subnetwork = data.google_compute_subnetwork.subnet.self_link
    secondary_range_blocks = {
      pods     = ""
      services = "/20" # can be an empty string as well
    }
  }
  logging_config = {
    enable_system_logs = false
  }
  monitoring_config = {
    enable_system_metrics     = false
    enable_managed_prometheus = false
  }
  enable_addons = {
    gce_persistent_disk_csi_driver = true
    gcp_filestore_csi_driver       = true
    gcs_fuse_csi_driver            = true
    dns_cache                      = true
    horizontal_pod_autoscaling     = true
  }
  enable_features = {
    gateway_api = true
  }
  depends_on = [
    google_project_service.project,
    google_project_iam_member.gke_service,
    google_project_iam_member.cloudservices,
  ]

}


resource "google_compute_firewall" "allow_istio_egress" {
  name        = "allow-istio-egress"
  project     = data.google_project.project.project_id
  network     = data.google_compute_network.vpc.self_link
  description = "Allow istio webhook egress"
  allow {
    protocol = "tcp"
    ports    = ["15000-16000"]
  }
  destination_ranges = ["0.0.0.0/0"]
  source_ranges      = ["0.0.0.0/0"]
  direction          = "EGRESS"
}

resource "google_compute_firewall" "allow_istio_ingress" {
  name        = "allow-istio-ingress"
  project     = data.google_project.project.project_id
  network     = data.google_compute_network.vpc.self_link
  description = "Allow istio webhook ingress"
  allow {
    protocol = "tcp"
    ports    = ["15000-16000"]
  }
  destination_ranges = ["0.0.0.0/0"]
  source_ranges      = ["0.0.0.0/0"]
  direction          = "INGRESS"
}
